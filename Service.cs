
using System;
using System.Threading;

namespace rqbnet
{

	public partial class Service
	{

		private int _wait;
		private bool _lock;
		private Thread _runner;
		private Settings _settings;

		public Service (Settings settings)
		{
			this.wait = settings.updateFrequency;
			this.settings = settings;
			this.stopped = true;
			this.runner = new Thread (new ThreadStart (this.run));
		}
		
	    public int wait 
	    {
	      get { return _wait; }
	      set { _wait = value; }
	    }
		
	    public Settings settings 
	    {
	      get { return _settings; }
	      set { _settings = value; }
		}
		
	    public bool stopped 
	    {
	      get { return _lock; }
	      set { _lock = value; }
	    }
		
	    public Thread runner 
	    {
	      get { return _runner; }
	      set { _runner = value; }
	    }	
				
		public void start()
		{
			this.stopped = false;
			this.runner.Start();
		}
		
		public void stop()
		{
			this.stopped = true;
		}
		
		public int getUpdateFrequency()
		{
			/*
			 * Cast second in millieconds
			 */
			return this.wait * 1000;
		}
		
		public void test()
		{
			while(!this.stopped) {
				Console.WriteLine ("Hello main-thread");
				Thread.Sleep (this.getUpdateFrequency());
			}
			if(this.stopped) {
				this.stop();
			}
		}		
		
		public void run()
		{
			while(!this.stopped) {
				Sync app = new Sync(this.settings);
				app.execute();
				Thread.Sleep (this.getUpdateFrequency());
			}
			if(this.stopped) {
				this.stop();
			}
		}		
		
	}
}
