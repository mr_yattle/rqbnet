using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;

namespace rqbnet
{
	public class Sync
	{
		/*
		Descrizione dei passi di dialogo:
		1) Lettura delle cartelle dei nasi
		2) Recupero informazioni su i files presenti per ogni naso
		3) Indicizzazione delle informazioni sui nasi mediante sqlite
		4) Upload dei file che necessitano di tale operazione
		*/
		private Settings _settings;
		public Sync(Settings settings )
		{
			this.settings = settings;
		}
		
	    public Settings settings 
	    {
	      get { return _settings; }
	      set { _settings = value; }
	    }
		
		public void _execute()
		{
			Settings settings = this.settings;
			this.archiveIndexes(settings);
			this.syncFiles(settings);
			Index index = this.retrieveUploadableFile(settings);
			if(index != null) {
				FileInfo file = new FileInfo(index.path);
				Uploader upload = new Uploader(settings);
				if(upload.send(index.path, index.naso, index.data)) {
					if(file.Length == index.dimensione) {
						index.stato = "terminated";
					}
					index.dimensione = file.Length;
					index.save(settings);
				}
			}
		}
		
		public void execute()
		{
			try {
				this._execute();
			}
			catch(Exception ex) {
				Log.fatal(ex.Message);
			}
		}
		
		public Index retrieveUploadableFile(Settings settings) {
			Indexes indexes = new Indexes(settings);
			return indexes.getUploadable();
		}
		
		public void syncFiles(Settings settings) 
		{
			foreach(string smellerDevice in settings.smellerDevices) {
				string smellerDevicePath = System.IO.Path.GetFullPath(settings.rootDir+"/"+smellerDevice);
				if(System.IO.Directory.Exists(smellerDevicePath)) { //Percorso della directory del naso
					
					// Indicizzazione dei file nella directory
					FileInfo[] oFiles = this.readTodayReports(smellerDevicePath);
					if(oFiles != null) {
						foreach(FileInfo file in oFiles) {
							Index findex = new Index(file, smellerDevice);
							findex.add(settings); // Indicizza gli elementi (gli inserisce se non sono già presenti)
							findex.sync(settings); // Aggiorna l'elemento se già esiste e se è cambioto l'hash
						}
					}
					
				}
				else {
					Log.info("La cartella "+smellerDevicePath+" non è presente");
				}
			}		
		}
		
		public void archiveIndexes(Settings settings) 
		{
			Indexes indexes = new Indexes(settings);
			List<Index> terminated = indexes.terminated(settings);
			foreach(Index index in terminated) {
				index.stato = "archived";
				index.save(settings);
			}
		}		
		
		public List<string> readSmellerDevices(Settings settings)
		{
			List<string> folders = new List<string>();
			foreach(string smellerDevice in settings.smellerDevices) {
				string smellerDevicePath = System.IO.Path.GetFullPath(settings.rootDir+"/"+smellerDevice);
				if(System.IO.Directory.Exists(smellerDevicePath)) {
					folders.Add(smellerDevicePath);
				}
				else {
					Log.info("La cartella "+smellerDevicePath+" non è presente");
				}
			}
			return folders;
		}
		
		public FileInfo[] readTodayReports(string directory)
		{
			// Monitoring/2009_10_11
			FileInfo[] oFiles = null;
			DateTime date = DateTime.Today;
			string today = String.Format("{0}_{1:d2}_{2:d2}", date.Year,date.Month,+date.Day);
			string reportsPath = System.IO.Path.GetFullPath(directory+"/Monitoring/"+today);
			if(System.IO.Directory.Exists(reportsPath)) {
				DirectoryInfo oDirectory = new DirectoryInfo(reportsPath);
				oFiles = oDirectory.GetFiles();
			}
			else {
				Log.info("La cartella "+reportsPath+" non è presente");
			}
			return oFiles;
		}		
		
	}
}
