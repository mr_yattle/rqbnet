
using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace rqbnet
{

	public class Hashing
	{
		private static byte[] _emptyBuffer = new byte[0];
		public Hashing()
		{
		}
		
		public static string HashString(FileInfo ofile)
		{
			Stream stream = ofile.OpenRead();
			byte[] data = CalculateMD5(stream);
			string ret = "";
	        for (int i=0; i < data.Length; i++)
	                ret += data[i].ToString("x2").ToLower();
	        return ret;
		}		
		
	    public static byte[] CalculateMD5(Stream stream)
	    {
	        return CalculateMD5(stream, 64 * 1024);
	    }
	
	    public static byte[] CalculateMD5(Stream stream, int bufferSize)
	    {
	        MD5 md5Hasher = MD5.Create();
	
	        byte[] buffer = new byte[bufferSize];
	        int readBytes;
	
	        while ((readBytes = stream.Read(buffer, 0, bufferSize)) > 0)
	        {
	            md5Hasher.TransformBlock(buffer, 0, readBytes, buffer, 0);
	        }
	
	        md5Hasher.TransformFinalBlock(_emptyBuffer, 0, 0);
	
	        return md5Hasher.Hash;
	    }
	}

}
