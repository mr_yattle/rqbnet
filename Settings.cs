using System;
using System.Configuration;
using System.IO;

namespace rqbnet
{

	public class Settings
	{

		private string _rootDir;
		private string _dataDir;
		private string[] _smellerDevices;
		private string _dataSource;
		private string _logLevel;
		private string _uploadPage;
		private string _emailTo;
		private string _emailFrom;
		private string _smtpServer;
		private string _proxyUri;
		private int _archiveDays;
		private int _updateFrequency;
		private bool _enableMailNotification;
		
		public Settings ()
		{
			this.rootDir = System.IO.Path.GetFullPath(ConfigurationManager.AppSettings["rootDir"]);
			this.smellerDevices = ConfigurationManager.AppSettings["smellerDevices"].Split(',');
			this.dataDir = ConfigurationManager.AppSettings["dataDir"];
			this.dataSource = ConfigurationManager.AppSettings["dataSource"];
			this.uploadPage = ConfigurationManager.AppSettings["uploadPage"];
			this.logLevel = ConfigurationManager.AppSettings["logLevel"];
			this.emailTo = ConfigurationManager.AppSettings["emailTo"];
			this.emailFrom = ConfigurationManager.AppSettings["emailFrom"];
			this.smtpServer = ConfigurationManager.AppSettings["smtpServer"];
			this.proxyUri = ConfigurationManager.AppSettings["proxyUri"];
			try {
				this.enableMailNotification = bool.Parse(ConfigurationManager.AppSettings["enableMailNotification"]);
			}
			catch(System.ArgumentNullException ex) {
				this.enableMailNotification = false;
			}
			try {
				this.archiveDays = int.Parse(ConfigurationManager.AppSettings["archiveDays"]);
			}
			catch(System.ArgumentNullException ex) {
				this.archiveDays = 0;
			}
			try {
				this.updateFrequency = int.Parse(ConfigurationManager.AppSettings["updateFrequency"]);
			}
			catch(System.ArgumentNullException ex) { // Default updateFrequency (seconds)
				this.updateFrequency = 3;
			}
		}
		
	    public int updateFrequency 
	    {
	      get { return _updateFrequency; }
	      set { _updateFrequency = value; }
	    }	
		
	    public bool enableMailNotification 
	    {
	      get { return _enableMailNotification; }
	      set { _enableMailNotification = value; }
	    }	
		
	    public string smtpServer 
	    {
	      get { return _smtpServer; }
	      set { _smtpServer = value; }
	    }
		
		public string proxyUri 
	    {
	      get { return _proxyUri; }
	      set { _proxyUri = value; }
	    }
		
	    public string emailTo 
	    {
	      get { return _emailTo; }
	      set { _emailTo = value; }
	    }	
		
	    public string emailFrom 
	    {
	      get { return _emailFrom; }
	      set { _emailFrom = value; }
	    }			
		
	    public string rootDir 
	    {
	      get { return _rootDir; }
	      set { _rootDir = value; }
	    }
		
	    public string dataDir 
	    {
	      get { return _dataDir; }
	      set { _dataDir = value; }
	    }

	    public string logLevel 
	    {
	      get { return _logLevel; }
	      set { _logLevel = value; }
	    }
		
	    public string[] smellerDevices 
	    {
	      get { return _smellerDevices; }
	      set { _smellerDevices = value; }
	    }
		
	    public string dataSource 
	    {
	      get { return _dataSource; }
	      set { _dataSource = value; }
	    }	
		
	    public string uploadPage 
	    {
	      get { return _uploadPage; }
	      set { _uploadPage = value; }
	    }	
		
	    public int archiveDays 
	    {
	      get { return _archiveDays; }
	      set { _archiveDays = value; }
	    }
		
	    public int addDays()
	    {
	    		int days = this.archiveDays;
			if(days > 0)
				return -days;
			return days;
	    }		
		
	}
}
