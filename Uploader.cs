
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Specialized;

namespace rqbnet
{


	public class Uploader
	{

		private WebClient client;
		private string page;
		private WebProxy proxy = null;
		private string userAgent = "RQBNet Client 1.0";
		
		public Uploader (Settings settings)
		{
			this.client = new WebClient();
			if(settings.proxyUri != null && settings.proxyUri != "") {
				// set proxy (if needed)
				this.proxy = new WebProxy(settings.proxyUri, true);
			}
			if(this.proxy != null) {
				this.client.Proxy = this.proxy;
				this.client.Headers["Expect"] = null;
	        		this.client.Headers["User-Agent"] = this.userAgent;
			}
			this.page = settings.uploadPage;
		}
		
		
		public bool send (string path, string device, DateTime data)
		{
			try {
				NameValueCollection myQueryStringCollection = new NameValueCollection();
				myQueryStringCollection.Add("data", data.ToString("d", Locale.it_IT));
				myQueryStringCollection.Add("device", device);
				this.client.QueryString = myQueryStringCollection;
				string content = "";
				/*
				if(this.proxy == null) {
					Log.info("Stai usando una connessione NO PROXY");
					byte[] responseArray = this.client.UploadFile(this.page, path);
					content = Encoding.UTF8.GetString(responseArray);
				}
				*/
				if(this.proxy == null) {
					Log.info("Stai usando una connessione NO PROXY");
					CookieContainer cookies = new CookieContainer();
					content = Uploader.HttpUploadFileEx(path, this.page, "file", 
					                                       "text/xml", myQueryStringCollection, 
					                                       cookies, this.proxy, this.userAgent);
				}
				else {
					Log.info("Stai usando una connessione con WEB " +
						"PROXY:" + this.proxy.Address.AbsoluteUri);
					CookieContainer cookies = new CookieContainer();
					content = Uploader.HttpUploadFileEx(path, this.page, "file", 
					                                       "text/xml", myQueryStringCollection, 
					                                       cookies, this.proxy, this.userAgent);
				}
				//Console.WriteLine(content);
				return true;
			}
			catch(System.ArgumentNullException ex) {
				Log.fatal("Non è stata definita la pagina per l'upload del file");
				return false;
			}
			catch(System.Net.WebException ex) {
				Log.fatal(ex.Message+" - è possibile che sia necessario impostare il Proxy se presente nella rete");
				try {
					WebResponse oResponse = ex.Response;
					Log.fatal(Uploader.readResponse(oResponse));
				}
				catch(Exception ex2) {
				}
				return false;
			}
			
		}
		
		public static string readResponse(WebResponse oResponse)
		{
				System.IO.Stream oStream = oResponse.GetResponseStream();
				string sResponse = "";
				int iRead = 0;
				while(true)
				{
					byte[] oBytes = new byte[2048];
					iRead = oStream.Read(oBytes, 0, 2048);
					if(iRead == 0) break;
					sResponse += Encoding.UTF8.GetString(oBytes);
				}
				return sResponse;	
		}
			
		public static string HttpUploadFileEx( string uploadfile, string url, 
		    string fileFormName, string contenttype,NameValueCollection querystring, 
		    CookieContainer cookies, WebProxy proxy, string userAgent)
		{
		    if( (fileFormName== null) ||
		        (fileFormName.Length ==0))
		    {
		        fileFormName = "file";
		    }
		
		    if( (contenttype== null) ||
		        (contenttype.Length ==0))
		    {
		        contenttype = "application/octet-stream";
		    }
		
		
		    string postdata;
		    postdata = "?";
		    if (querystring!=null)
		    {
		        foreach(string key in querystring.Keys)
		        {
		            postdata+= key +"=" + querystring.Get(key)+"&";
		        }
		    }
		    Uri uri = new Uri(url+postdata);
		
		
		    string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
		    HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
			if(proxy != null)
				webrequest.Proxy = proxy;
			
			webrequest.ServicePoint.Expect100Continue = false; // If Expect: 100-Continue get error from proxy
		    webrequest.CookieContainer = cookies;
			webrequest.UserAgent = userAgent;
		    webrequest.ContentType = "multipart/form-data; boundary=" + boundary;
		    webrequest.Method = "POST";
		
		
		    // Build up the post message header
		    StringBuilder sb = new StringBuilder();
		    sb.Append("--");
		    sb.Append(boundary);
		    sb.Append("\r\n");
		    sb.Append("Content-Disposition: form-data; name=\"");
		    sb.Append(fileFormName);
		    sb.Append("\"; filename=\"");
		    sb.Append(Path.GetFileName(uploadfile));
		    sb.Append("\"");
		    sb.Append("\r\n");
		    sb.Append("Content-Type: ");
		    sb.Append(contenttype);
		    sb.Append("\r\n");
		    sb.Append("\r\n");            
		
		    string postHeader = sb.ToString();
		    byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);
		
		    // Build the trailing boundary string as a byte array
		
		    // ensuring the boundary appears on a line by itself
		
		    byte[] boundaryBytes = 
		           Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
		
		    FileStream fileStream = new FileStream(uploadfile, 
		                                FileMode.Open, FileAccess.Read);
		    long length = postHeaderBytes.Length + fileStream.Length + 
		                                           boundaryBytes.Length;
		    webrequest.ContentLength = length;
		
		    Stream requestStream = webrequest.GetRequestStream();
		
		    // Write out our post header
		
		    requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);
		
		    // Write out the file contents
		
		    byte[] buffer = new Byte[checked((uint)Math.Min(4096, 
		                             (int)fileStream.Length))];
		    int bytesRead = 0;
		    while ( (bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0 )
		        requestStream.Write(buffer, 0, bytesRead);
		
		    // Write out the trailing boundary
		
		    requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
		    WebResponse responce = webrequest.GetResponse();
		    Stream s = responce.GetResponseStream();
		    StreamReader sr = new StreamReader(s);
		
		    return sr.ReadToEnd();
		}		
		
		
	}
}
