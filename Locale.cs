
using System;
using System.Globalization;

namespace rqbnet
{


	public class Locale
	{
		private CultureInfo _culture;
		public Locale ()
		{
			this.culture = new CultureInfo("it-IT", true);
			this.culture.DateTimeFormat.DateSeparator = "/";
			this.culture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";	
		}
		
	    public CultureInfo culture 
	    {
	      get { return _culture; }
	      set { _culture = value; }
	    }
		
	    public static CultureInfo it_IT 
	    {
	      get {
			Locale locale = new Locale();
			return locale.culture;
		  }
	    }
		
	}
}
