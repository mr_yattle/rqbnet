using System;
using System.ServiceProcess;


namespace rqbnet
{
	public partial class MainClass : ServiceBase
	{
		
		private Settings _settings;
		private Service _service;
		public MainClass ()
		{
			this.ServiceName = "RQBNet Uploader Service";
            this.EventLog.Log = "Application";
            
            // These Flags set whether or not to handle that specific
            //  type of event. Set to true if you need it, false otherwise.
            this.CanHandlePowerEvent = false;
            this.CanHandleSessionChangeEvent = false;
            this.CanPauseAndContinue = false;
            this.CanShutdown = false;
            this.CanStop = true;
			
			// Init operation classes
			this.settings = new Settings();
			this.service = new Service(settings);
		}		
		
	    public Settings settings 
	    {
	      get { return _settings; }
	      set { _settings = value; }
		}
		
	    public Service service 
	    {
	      get { return _service; }
	      set { _service = value; }
		}
		
		public static void Main (string[] args)
		{
			ServiceBase.Run(new MainClass());
		}
		
		public static void _Main (string[] args)
		{
			/*
			 * Test as thread
			MainClass app = new MainClass();
			app.service.start();
			*/
			Settings settings = new Settings();
			Sync app = new Sync(settings);
			app.execute();
		}
		
        protected override void OnStart(string[] args)
        {
			Log.info("starting service ....");
			this.service.start();
        }

        protected override void OnStop()
        {
			Log.info("stopping service ....");
			this.service.stop();
        }
		
	}
}
