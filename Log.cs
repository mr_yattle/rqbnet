
using System;
using System.IO;
using System.Web.Mail;

namespace rqbnet
{


	public class Log
	{
		private FileInfo _log;
		private StreamWriter _stream;
		private Settings _settings;
		public Log ()
		{
			this.settings = new Settings();
			string logfile = "rqbnet.log";
			if(System.IO.Directory.Exists(System.IO.Path.GetFullPath(this.settings.dataDir))) {
				logfile = System.IO.Path.GetFullPath(this.settings.dataDir+"/rqbnet.log");
			}
			this.log = new FileInfo(logfile);
			if(!this.log.Exists) {
				this.stream = this.log.CreateText();
				this.stream.Close();
			}
		}
		
	    public Settings settings 
	    {
	      get { return _settings; }
	      set { _settings = value; }
	    }		
		
	    public FileInfo log 
	    {
	      get { return _log; }
	      set { _log = value; }
	    }
		
	    public StreamWriter stream 
	    {
	      get { return _stream; }
	      set { _stream = value; }
	    }	
		

		public bool SendEmail(string emailTo, string emailFrom, string emailBody, string emailSubject, string emailSmtpServer)
		{
			if(this.settings.enableMailNotification) {
				try
				{
					MailMessage mail = new MailMessage();
					mail.To = emailTo;
					mail.From = emailFrom;
					mail.Subject = emailSubject;
					mail.Body = emailBody;
					mail.BodyFormat = MailFormat.Html;
					SmtpMail.SmtpServer = emailSmtpServer;
					SmtpMail.Send(mail);
					return true;
				}
				catch(Exception ex)
				{
					string msg = String.Format("MailSender ({0})", ex.Message);
					Log.info(msg);
					return false;
				}
			}
			else {
				Log.info("Mail notification is not enabled");
			}
			return false;
		}		
		
		
	    public void save(string value)
	    {
			try {
				this.stream = File.AppendText(this.log.ToString());
				this.stream.WriteLine(value);
				this.stream.Close();
			}	
			catch(System.IO.IOException ioex) {
				// do nothing probably log file is unaccessible cause user is reading it
			}
	    }
		
		public static void debug(string value)
		{
			
			Log oLog = new Log();
			if(oLog.settings.logLevel == "debug") {
				string log = String.Format("{0} - DEBUG: {1}", DateTime.Now.ToString(Locale.it_IT), value);
				oLog.save(log);
			}
		}		
		
		public static void info(string value)
		{
			Log oLog = new Log();
			if(oLog.settings.logLevel != "fatal") {
				string log = String.Format("{0} - INFO: {1}", DateTime.Now.ToString(Locale.it_IT), value);
				oLog.save(log);
			}
		}
		
		public static void fatal(string value)
		{
			Log oLog = new Log();
			string log = String.Format("{0} - FATAL: {1}", DateTime.Now.ToString(Locale.it_IT), value);
			
			string emailTo = oLog.settings.emailTo;
			string emailFrom = oLog.settings.emailFrom;
			string emailBody = log;
			string emailSubject = "RQNet Reports Uploader";
			string emailSmtpServer = oLog.settings.smtpServer;
			oLog.SendEmail(emailTo, emailFrom, emailBody, emailSubject, emailSmtpServer);
			oLog.save(log);
		}
		
	}
}
