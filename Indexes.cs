
using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using Mono.Data.SqliteClient;
using System.Text;

namespace rqbnet
{
	/* (TABELLA DELLE INFORMAZIONI SUL FILE)
	 * CREATE TABLE "rapporti" ("nome" VARCHAR NOT NULL , 
	 * "data" DATETIME NOT NULL , 
	 * "dimensione" FLOAT NOT NULL , 
	 * "stato" VARCHAR NOT NULL , 
	 * "path" TEXT NOT NULL  check(typeof("path") = 'text') , 
	 * "hash" VARCHAR NOT NULL, 
	 * "naso" VARCHAR NOT NULL, 
	 * PRIMARY KEY ("nome", "data"))
	 * */

	public class Index
	{
		private string _nome;
		private float _dimensione;
		private string _stato;
		private string _path;
		private string _hash;
		private string _naso;
		private DateTime _data;
		
	    public string nome 
	    {
	      get { return _nome; }
	      set { _nome = value; }
	    }	
		
	    public DateTime data 
	    {
	      get { return _data; }
	      set { _data = value; }
	    }
		
	    public float dimensione 
	    {
	      get { return _dimensione; }
	      set { _dimensione = value; }
	    }	
		
	    public string stato 
	    {
	      get { return _stato; }
	      set { _stato = value; }
	    }
		
	    public string path 
	    {
	      get { return _path; }
	      set { _path = value; }
	    }
		
		public string hash 
	    {
	      get { return _hash; }
	      set { _hash = value; }
	    }
		
		public string naso 
	    {
	      get { return _naso; }
	      set { _naso = value; }
	    }
		
		public static string calculateHash(FileInfo oFile)
		{
			string hash_string = Hashing.HashString(oFile);
			return hash_string;
		}
		
	    public Index(IDataReader reader) 
	    {
			this.nome = reader.GetString(0);
			this.data = DateTime.Parse(reader.GetString(1));
			this.dimensione = reader.GetFloat(2);
			this.stato = reader.GetString(3);
			this.path = reader.GetString(4);
			this.hash = reader.GetString(5);
			this.naso = reader.GetString(6);
	    }
		
	    public Index(FileInfo ofile, string naso) 
	    {
			this.nome = ofile.Name;
			this.data = DateTime.Today;
			this.dimensione = ofile.Length;
			this.stato = "indexing";
			this.path = ofile.FullName;
			this.hash = Index.calculateHash(ofile);
			this.naso = naso;
	    }
		
	    public Index save(Settings settings) 
	    {
			Indexes indexes = new Indexes(settings);
			return indexes.update(this);
	    }
		
	    public Index add(Settings settings) 
	    {
			Indexes indexes = new Indexes(settings);
			return indexes.insert(this);
	    }
		
	    public bool sync(Settings settings) 
	    {
			Indexes indexes = new Indexes(settings);
			indexes.sync(this);
			return true;
	    }
		
	}
	
	public class Term
	{
		private string _colonna;
		private string _operatore;
		private string _valore_stringa;
		private DateTime _valore_data;
		public Term(string colonna, string operatore, string valore)
		{
			this.colonna = colonna;
			this.operatore = operatore;
			this.valore_stringa = valore;
		}
		
		public Term(string colonna, string operatore, DateTime valore)
		{
			this.colonna = colonna;
			this.operatore = operatore;
			this.valore_data = valore;
		}
		
	    public string colonna 
	    {
	      get { return _colonna; }
	      set { _colonna = value; }
	    }		
		
	    public string operatore 
	    {
	      get { return _operatore; }
	      set { _operatore = value; }
	    }
		
	    public string valore_stringa 
	    {
	      get { return _valore_stringa; }
	      set { _valore_stringa = value; }
	    }
		
	    public DateTime valore_data 
	    {
	      get { return _valore_data; }
	      set { _valore_data = value; }
	    }		
		
	    public string valore() 
	    {
			if(this.valore_data != null && this.valore_stringa == null)
				return this.valore_data.ToString();
			return this.valore_stringa;
	    }	
		
	}
	
	public class SearchTerms
	{
		private List<Term> terms;
		public SearchTerms() 
	    {
			this.terms = new List<Term>();
	    }
		
		public void Add(string column, string op, string value)
		{
			Term term = new Term(column, op, value);
			this.terms.Add(term);
		}
		
		public void Add(string column, string op, DateTime value)
		{
			Term term = new Term(column, op, value);
			this.terms.Add(term);
		}
		
		public string getSQLWhere() 
		{
			string sql = "";
			foreach(Term term in this.terms) {
				sql += String.Format(" AND {0} {1} '{2}'", 
				                     term.colonna, term.operatore, term.valore());
			}
			return sql;
		}
	}
	
	public class Indexes
	{
		private string _connectionString;
		private IDbConnection _dbcon;
		
	    public string connectionString 
	    {
	      get { return _connectionString; }
	      set { _connectionString = value; }
	    }
		
	    public IDbConnection dbcon 
	    {
	      get { return _dbcon; }
	      set { _dbcon = value; }
	    }		
		
		public Indexes (Settings settings, bool connect)
		{
			this.init(settings, connect);
		}
		
		public Indexes(Settings settings) 
		{
			bool connect = true;
			this.init(settings, connect);
		}
		
		public void initConnectionString(Settings settings)
		{
				string dataSource = settings.dataDir+"/"+settings.dataSource;
				if(!System.IO.File.Exists(System.IO.Path.GetFullPath(dataSource))) {
					throw new System.IO.FileNotFoundException("Il file sqlite " +
						"specificato nei settings:<"+settings.dataSource+"> non esiste");
				}
				string connectionURI = "URI=file:"+System.IO.Path.GetFullPath(dataSource);
				this.connectionString = connectionURI;
		}
		
		public void init(Settings settings, bool connect) {
			try {
				this.initConnectionString(settings);
				this.connect();
			} catch (System.IO.FileNotFoundException ex) {
		    		Log.fatal(ex.Message);
			} catch (System.ArgumentNullException ex) {
		    		Log.fatal("Manca il parametro dataSource nel file di configurazione");
			}
		}
		
		public void connect()
		{
       		this.dbcon = (IDbConnection) new SqliteConnection(this.connectionString);
       		this.dbcon.Open();
		}
		
		public void tests()
		{
       		//this.find();
		}
		
		public Index findById(string name, DateTime data)
		{
			List<Index> indexes = this.find(name, data);
			if(indexes.Count > 0) {
				return indexes[0];
			}
			return null;
		}
		
		public void stopInProgress() {
			/*
			 * Aggiorno tutti i processi in fase di upload
			 */
			IDbCommand dbcmd = this.dbcon.CreateCommand();
			string sql = "UPDATE rapporti set stato = 'indexing' WHERE stato = 'in progress'";
			Log.debug(sql);
			dbcmd.CommandText = sql;
			dbcmd.ExecuteNonQuery();
       		dbcmd = null;
		}
		
		public Index getUploadable()
		{
       		/*
       		 * Informazioni sul file
       		 * check order
       		 * 1) in progress
       		 * 2) il file con la data più vecchia in stato indexing
       		 */
			IDbCommand dbcmd = this.dbcon.CreateCommand();
	       	string sql = "SELECT nome, data, dimensione, stato, path, hash, naso " +
	       		"FROM rapporti WHERE stato = 'in progress'";
			Log.debug(sql);
	       	dbcmd.CommandText = sql;
	       	IDataReader reader = dbcmd.ExecuteReader();
			while(reader.Read()) {
				return new Index(reader);
       		}
			
			// Nel caso non ci siano file in progress prendo il primo in fase di indexing e lo metto instato in progress
			sql = "SELECT nome, data, dimensione, stato, path, hash, naso " +
				"FROM rapporti WHERE stato == 'indexing' ORDER BY data, nome ASC";
			Log.debug(sql);
			dbcmd.CommandText = sql;
	       	reader = dbcmd.ExecuteReader();
			while(reader.Read()) {
				Index index = new Index(reader);
				index.stato = "in progress";
				this.update(index);
				return index;
       		}
			
			// clean up
       		reader.Close();
       		reader = null;
       		dbcmd.Dispose();
       		dbcmd = null;
			return null;
		}
		
		public List<Index> terminated(Settings settings)
		{
			SearchTerms searchTerms = new SearchTerms();
			searchTerms.Add("stato", "=", "terminated");
			DateTime today = DateTime.Today.AddDays(settings.addDays());
			searchTerms.Add("data", "<", today);
			return this.find(searchTerms);
		}
		
		public List<Index> find(string nome, DateTime data)
		{
			SearchTerms keys = new SearchTerms();
			keys.Add("nome", "=", nome);
			keys.Add("data", "=", data);
			return this.find(keys);
		}		
		
		public List<Index> find(SearchTerms terms)
		{
			List<Index> indexes = new List<Index>();
			IDbCommand dbcmd = this.dbcon.CreateCommand();
	       	string sql = "SELECT nome, data, dimensione, stato, path, hash, naso FROM rapporti WHERE 1";
			sql += terms.getSQLWhere();
			Log.debug(sql);
	       	dbcmd.CommandText = sql;
	       	IDataReader reader = dbcmd.ExecuteReader();
			while(reader.Read()) {
				Index index = new Index(reader);
				indexes.Add(index);
       		}
			// clean up
       		reader.Close();
       		reader = null;
       		dbcmd.Dispose();
       		dbcmd = null;
			return indexes;
		}
		
		public Index insert(Index index)
		{
			if(this.findById(index.nome, index.data) != null) // Record already present
				return index;
			
			IDbCommand dbcmd = this.dbcon.CreateCommand();
			string sql = String.Format("INSERT INTO rapporti (nome, data, dimensione, stato, path, hash, naso) " +
				"VALUES('{0}', '{1}', {2}, '{3}', '{4}', '{5}', '{6}')", 
				index.nome, index.data, index.dimensione, index.stato, index.path, index.hash, index.naso);
			Log.debug(sql);
			dbcmd.CommandText = sql;
			try {
				dbcmd.ExecuteNonQuery();
			}
			catch(Mono.Data.SqliteClient.SqliteExecutionException ex) { // Primary key violation
				string error = ex.Message;
				if(error.IndexOf("SQL logic error") == -1) {
					Log.fatal(error);
				}
			}
       		dbcmd = null;
			return index;
		}
		
		public Index update(Index index)
		{
			IDbCommand dbcmd = this.dbcon.CreateCommand();
			string sql = String.Format("UPDATE rapporti set nome = '{0}', " +
				"data = '{1}', " +
				"dimensione = {2}, " +
				"stato = '{3}', " +
				"path = '{4}', " +
			    "hash = '{5}'," +
			    "naso = '{6}'" +                      
				"WHERE nome = '{0}' AND data = '{1}'", 
				index.nome, index.data, index.dimensione, index.stato, index.path, index.hash, index.naso);
			Log.debug(sql);
			dbcmd.CommandText = sql;
			try {
				dbcmd.ExecuteNonQuery();
			}
			catch(Mono.Data.SqliteClient.SqliteExecutionException ex) { // Primary key violation
				string error = ex.Message;
				if(error.IndexOf("SQL logic error") != -1) {
					Log.fatal("E possibile che il file sql sia in sola lettura o che ci sia un LOCK nel database");
				}
				else {
					Log.fatal(error);
				}
			}
       		dbcmd = null;
			return index;
		}	
		
		
		public bool sync(Index file)
		{
			/*
			 * Sincronizzo i file modificati dall'RQNet sul disco
			 * Evito di modificare gli indici dei reports in fase di trasmissione via http
			 */
			Index row = this.findById(file.nome, file.data);
			if(row != null) {
				if(row.hash != file.hash && row.stato != "in progress") {
					file.stato = "indexing";
					this.update(file);
					return true;
				}
			}
			return false;
		}	
		
	    // Class destructor
	    ~ Indexes()
	    {
			try {
		    		this.dbcon.Close();
				this.dbcon = null;
		    }
		    catch (Exception ex) {
		    		Log.info(ex.Message);
			}
	    }
		
	}
}
